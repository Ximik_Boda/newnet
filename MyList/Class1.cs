﻿using System.Collections.Generic;
using System.Collections;
using System;

namespace MyList
{
	public class OnAddDeleteEventArgs<T> : EventArgs
	{
		public int index;
		public T el;
	}
	public class OnChangeOffsetEventArgs : EventArgs
	{
		public int offset;
	}

	public class MyList<T> : ICollection, IEnumerable
	{
		private T[] items = null;
		private int size = 0;
		private int offset = 0;

		public delegate void OnClearDelegate(object sender);
		public event OnClearDelegate OnClear = null;

		public delegate void OnAddDelegate(object sender, OnAddDeleteEventArgs<T> e);
		public event OnAddDelegate OnAdd = null;

		public delegate void OnDeleteDelegate(object sender, OnAddDeleteEventArgs<T> e);
		public event OnDeleteDelegate OnDelete = null;

		public delegate void OnAccesToFirstItemDelegate(object sender);
		public event OnAccesToFirstItemDelegate OnAccesToFirstItem = null;

		public delegate void OnAccesToLastItemDelegate(object sender);
		public event OnAccesToLastItemDelegate OnAccesToLastItem = null;

		public delegate void OnChangeOffsetDelegate(object sender, OnChangeOffsetEventArgs e);
		public event OnChangeOffsetDelegate OnChangeOffset = null;

		public MyList(int start_index = 0)
		{
			items = new T[0];
			size = 0;
			this.offset = start_index;
		}

		public MyList(MyList<T> list)
		{
			if (list == null)
				throw new ArgumentNullException();
			size = list.size;
			items = new T[size];
			list.CopyTo(items, 0);
			offset = list.offset;
		}

		public void Add(T value)
		{
			if (OnAdd != null) {
				OnAddDeleteEventArgs<T> e = new OnAddDeleteEventArgs<T>();
				e.el = value;
				e.index = size + offset;
				OnAdd(this, e);
			}

			T[] temp = new T[size + 1];
			items.CopyTo(temp, 0);
			items = temp;
			temp[size] = value;
			size++;
		}

		public void AddRange(IEnumerable<T> collection)
		{
			foreach (T v in collection)
			{
				Add(v);
			}
		}

		public void Insert(int index, T item)
		{
			index -= offset;
			if (index < 0 || index > size)
			{
				throw new ArgumentOutOfRangeException();
			}

			if (OnAdd != null)
			{
				OnAddDeleteEventArgs<T> e = new OnAddDeleteEventArgs<T>();
				e.el = item;
				e.index = index + offset;
				OnAdd(this, e);
			}

			T[] temp = new T[size + 1];

			for (int i = 0; i < index; ++i)
				temp[i] = items[i];

			temp[index] = item;

			for (int i = index + 1; i < size + 1; ++i)
				temp[i] = items[i - 1];

			items = temp;
			size++;
		}

		public void InsertRange(int index, IEnumerable<T> collection)
		{
			index -= offset;
			if (index < 0 || index > size)
			{
				throw new ArgumentOutOfRangeException();
			}

			int i = index;

			foreach (T v in collection)
			{
				Insert(i + offset, v);
				++i;
			}
		}

		public bool Remove(T item)
		{
			int ind = -1;
			for (int i = 0; i < size; ++i)
				if (items[i].Equals(item))
				{
					ind = i;
					break;
				}
			if (ind == -1)
				return false;

			RemoveAt(ind + offset);
			return true;
		}

		public void RemoveAt(int index)
		{
			index -= offset;
			if (index < 0 || index >= size)
			{
				throw new ArgumentOutOfRangeException();
			}

			if (OnDelete != null)
			{
				OnAddDeleteEventArgs<T> e = new OnAddDeleteEventArgs<T>();
				e.el = items[index];
				e.index = index + offset;
				OnDelete(this, e);
			}

			T[] temp = new T[size - 1];

			for (int i = 0; i < index; ++i)
				temp[i] = items[i];

			for (int i = index + 1; i < size; ++i)
				temp[i - 1] = items[i];

			items = temp;
			size--;
		}

		public void RemoveRange(int index, int count)
		{
			index -= offset;
			if (index < 0 || index >= size || count < 0 || index + count >= size)
			{
				throw new ArgumentOutOfRangeException();
			}
			for (int i = 0; i < count; ++i)
				RemoveAt(index + offset);
		}

		public int RemoveAll(Predicate<T> match)
		{
			if (match == null)
				throw new ArgumentNullException();

			int count = 0;

			for (int i = 0; i < size; ++i)
				if (match(items[i]))
				{
					RemoveAt(i+offset);
					--i;
					++count;
				}

			return count;
		}

		public void Clear()
		{
			if (OnClear != null)
				OnClear(this);

			items = new T[0];
			size = 0;
		}

		public MyList<T> GetRange(int index, int count)
		{
			index -= offset;
			if (index < 0 || index >= size || count < 0 || index + count > size)
			{
				throw new ArgumentOutOfRangeException();
			}
			MyList<T> temp = new MyList<T>(index + offset);
			for (int i = 0; i < count; ++i)
				temp.Add(items[i + index]);
			return temp;
		}

		public bool Contains(T item)
		{
			for (int i = 0; i < size; ++i)
				if (items[i].Equals(item))
					return true;
			return false;
		}

		public MyList<T> FindAll(Predicate<T> match)
		{
			if (match == null)
				throw new ArgumentNullException();

			MyList<T> temp = new MyList<T>(0);

			for (int i = 0; i < size; ++i)
				if (match(items[i]))
				{
					temp.Add(items[i]);
				}

			return temp;
		}

		public T[] ToArray() {
			T[] array = new T[size];
			CopyTo(array, 0);
			return array;
		}

		public T this[int index]
		{
			get
			{
				index -= offset;

				if (index < 0 || index >= size)
				{
					throw new ArgumentOutOfRangeException();
				}

				if (index == 0 && OnAccesToFirstItem != null)
				{
					OnAccesToFirstItem(this);
				}
				if (index == size - 1 && OnAccesToLastItem != null)
				{
					OnAccesToLastItem(this);
				}

				return items[index];
			}
			set
			{
				index -= offset;

				if (index < 0 || index >= size)
				{
					throw new ArgumentOutOfRangeException();
				}

				if (index == 0 && OnAccesToFirstItem != null)
				{
					OnAccesToFirstItem(this);
				}
				if (index == size - 1 && OnAccesToLastItem != null)
				{
					OnAccesToLastItem(this);
				}

				items[index] = value;
			}
		}


		public void CopyTo(Array array, int index)
		{
			foreach (T val in items)
			{
				array.SetValue(val, index);
				index++;
			}
		}

		public int Count
		{
			get
			{
				return size;
			}
		}

		public int Capacity
		{
			get
			{
				return size;
			}
		}

		public int Offset
		{
			get
			{
				return offset;
			}
			set
			{

				if (OnChangeOffset != null)
				{
					OnChangeOffsetEventArgs e = new OnChangeOffsetEventArgs();
					e.offset = value;
					OnChangeOffset(this, e);
				}
				offset = value;
			}
		}

		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		public IEnumerator GetEnumerator()
		{
			return new Enumerator(items);
		}

		public struct Enumerator : /*IEnumerator<T>,*/ IEnumerator
		{
			private T[] items;
			private int cursor;

			public Enumerator(T[] items)
			{
				this.items = items;
				cursor = -1;
			}

			public void Reset()
			{
				cursor = -1;
			}

			public bool MoveNext()
			{
				if (cursor < items.Length)
					cursor++;

				return (!(cursor == items.Length));
			}

			object IEnumerator.Current
			{
				get
				{
					if ((cursor < 0) || (cursor >= items.Length))
					{
						throw new InvalidOperationException();
					}
					return items[cursor];
				}
			}

		}
	}
}
