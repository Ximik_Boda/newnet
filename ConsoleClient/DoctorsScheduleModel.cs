﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient
{
	public class DoctorsScheduleModel
	{
		public int? ScheduleId;

		public DateTime? Date;

		public int? DoctorId;
		public int? PatientId;
	}
}
