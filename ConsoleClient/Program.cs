﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient
{
	class Program
	{
		private const string path = @"https://localhost:44370";
		static void Main(string[] args)
		{
			Console.Write("Try get all doctors...");
			using (var client = new HttpClient())
			{
				var response = client.GetAsync(path + "/api/Doctor").Result;
				Console.WriteLine("done, status code = " + response.StatusCode.ToString());
				var rez = response.Content.ReadAsAsync<List<DoctorModel>>().Result;
				foreach (var p in rez)
					Console.WriteLine(p.DoctorId + " " + p.Surname +" "+ p.Name +" "+ p.Patronymic);
			}

			DoctorModel test_doctor = new DoctorModel() { DoctorId = null, Surname = "Ivanov", Name = "Ivan", Patronymic = "Ivanovich" };

			Console.WriteLine();
			Console.Write("Try to post doctor...");
			using (var client = new HttpClient())
			{
				var response = client.PostAsJsonAsync(path + "/api/Doctor", test_doctor).Result;
				Console.WriteLine("done, status code = " + response.StatusCode.ToString());
				var rez = response.Content.ReadAsAsync<DoctorModel>().Result;
				
				Console.WriteLine(rez.DoctorId + " " + rez.Surname + " " + rez.Name + " " + rez.Patronymic);

				test_doctor = rez;
			}

			Console.WriteLine();
			Console.Write("Try to get new doctor...");
			using (var client = new HttpClient())
			{
				var response = client.GetAsync(path + "/api/Doctor/" + test_doctor.DoctorId.ToString()).Result;
				Console.WriteLine("done, status code = " + response.StatusCode.ToString());
				var rez = response.Content.ReadAsAsync<DoctorModel>().Result;

				Console.WriteLine(rez.DoctorId + " " + rez.Surname + " " + rez.Name + " " + rez.Patronymic);
			}
			

			

			test_doctor.Name = "Igor";

			Console.WriteLine();
			Console.Write("Try to put doctor changes...");
			using (var client = new HttpClient())
			{
				var response = client.PutAsJsonAsync(path + "/api/Doctor", test_doctor).Result;
				Console.WriteLine("done, status code = " + response.StatusCode.ToString());
				var rez = response.Content.ReadAsAsync<DoctorModel>().Result;

				Console.WriteLine(rez.DoctorId + " " + rez.Surname + " " + rez.Name + " " + rez.Patronymic);

				test_doctor = rez;
			}

			Console.WriteLine();
			Console.Write("Try get all doctors 2...");
			using (var client = new HttpClient())
			{
				var response = client.GetAsync(path + "/api/Doctor").Result;
				Console.WriteLine("done, status code = " + response.StatusCode.ToString());
				var rez = response.Content.ReadAsAsync<List<DoctorModel>>().Result;
				foreach (var p in rez)
					Console.WriteLine(p.DoctorId + " " + p.Surname + " " + p.Name + " " + p.Patronymic);
			}

			//int doctor_shedule_id;

			DoctorsScheduleModel test_schedule = new DoctorsScheduleModel() { DoctorId = test_doctor.DoctorId };

			Console.WriteLine();
			Console.Write("Try to post doctor schedule...");
			using (var client = new HttpClient())
			{
				//(int DoctorId, int? PatientId) tmp = ((int)test_doctor.DoctorId, null);
				//DoctorsScheduleModel tmp = 
				var response = client.PostAsJsonAsync(path + "/api/DoctorsSchedule", test_schedule).Result;
				Console.WriteLine("done, status code = " + response.StatusCode.ToString());
				var rez = response.Content.ReadAsStringAsync().Result;

				Console.WriteLine(rez);

				test_schedule = response.Content.ReadAsAsync<DoctorsScheduleModel>().Result;
			}

			Console.WriteLine();
			Console.Write("Try to get schedule of new doctor...");
			using (var client = new HttpClient())
			{
				var response = client.GetAsync(path + "/api/Doctor/" + test_doctor.DoctorId.ToString()+ "/schedule").Result;
				Console.WriteLine("done, status code = " + response.StatusCode.ToString());
				var rez = response.Content.ReadAsStringAsync().Result;

				Console.WriteLine(rez);
			}


			Console.WriteLine();
			Console.Write("Try to delete doctor...");
			using (var client = new HttpClient())
			{
				var response = client.DeleteAsync(path + "/api/Doctor/" + test_doctor.DoctorId.ToString()).Result;
				Console.WriteLine("done, status code = " + response.StatusCode.ToString());
			}

			Console.WriteLine();
			Console.Write("Try get all doctors schedulus...");
			using (var client = new HttpClient())
			{
				var response = client.GetAsync(path + "/api/DoctorsSchedule").Result;
				Console.WriteLine("done, status code = " + response.StatusCode.ToString());
				var rez = response.Content.ReadAsStringAsync().Result;
				Console.WriteLine(rez);
			}

			//Console.WriteLine();
			//Console.Write("Try to delete doctor schedule...");
			//using (var client = new HttpClient())
			//{
			//	var response = client.DeleteAsync(path + "/api/DoctorsSchedule/" + test_schedule.ScheduleId.ToString()).Result;
			//	Console.WriteLine("done, status code = " + response.StatusCode.ToString());
			//}

			Console.ReadKey();
		}
	}
}
