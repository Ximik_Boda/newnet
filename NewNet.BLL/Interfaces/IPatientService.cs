﻿using NewNet.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.BLL.Interfaces
{
	public interface IPatientService<PatientDTO> : IService<PatientDTO>
	{
		IEnumerable<DoctorsScheduleDTO> GetSchedule(int id);

		IEnumerable<AttendanceCardDTO> GetAttendanceCard(int id);

		public IEnumerable<PatientDTO> FindByText(string text);
	}
}
