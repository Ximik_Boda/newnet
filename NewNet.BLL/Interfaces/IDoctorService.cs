﻿using NewNet.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.BLL.Interfaces
{
	public interface IDoctorService<DoctorDTO> : IService<DoctorDTO>
	{
		void AddScheduleElement(DoctorDTO doctor, DoctorsScheduleDTO element);
		IEnumerable<DoctorsScheduleDTO> GetSchedule(int id);
		IEnumerable<DoctorsScheduleDTO> GetReceptionSchedule(int id);
		IEnumerable<DoctorsScheduleDTO> GetFreeSchedule(int id);

		IEnumerable<AttendanceCardDTO> GetAttendanceCard(int id);

		public IEnumerable<DoctorDTO> FindByText(string text);
	}
}
