﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewNet.BLL.DTO;

namespace NewNet.BLL.Interfaces
{
	public interface IDoctorsScheduleService<DoctorsScheduleDTO> : IService<DoctorsScheduleDTO>
	{
		DoctorsScheduleDTO PatientUser(int id, int patient_id);
		void DeletePatient(int id);
	}
}
