﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.BLL.DTO
{
	public class DoctorsScheduleDTO
	{
		public int? ScheduleId { get; set; }

		public DateTime? Date { get; set; }

		public int? DoctorId { get; set; }
		public int? PatientId { get; set; }
		
	}
}
