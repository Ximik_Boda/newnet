﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.BLL.DTO
{
	public class PatientDTO
	{
		public int? PatientId { get; set; }

		public string Surname { get; set; }
		public string Name { get; set; }
		public string Patronymic { get; set; }
	}
}
