﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.BLL.DTO
{
	public class AttendanceCardDTO
	{
		public int? AttendanceCardId { get; set; }

		public DateTime? Date { get; set; }

		public string Diagnos { get; set; }

		public int? DoctorId { get; set; }
		public int? PatientId { get; set; }
	}
}
