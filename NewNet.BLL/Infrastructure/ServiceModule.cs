﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NewNet.DAL.Context;
using NewNet.DAL.Interfaces;
using NewNet.DAL.Repositories;
using Ninject.Modules;

namespace NewNet.BLL.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        private DbContextOptions<HospitalRegistryContext> options;

        public ServiceModule(DbContextOptions<HospitalRegistryContext> _options)
        {
            options = _options;
        }

        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(options);
        }
    }
}
