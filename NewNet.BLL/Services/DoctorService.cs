﻿using AutoMapper;
using NewNet.BLL.DTO;
using NewNet.BLL.Infrastructure;
using NewNet.BLL.Interfaces;
using NewNet.DAL.Entities;
using NewNet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.BLL.Services
{
	public class DoctorService : IDoctorService<DoctorDTO>
	{
		IUnitOfWork DataBase { get; set; }

		IMapper mapperDTOFromEntity;
		IMapper mapperDTOToEntity;

		public DoctorService(IUnitOfWork uow)
		{
			DataBase = uow;
			mapperDTOFromEntity = new MapperConfiguration(cfg => cfg.CreateMap<Doctor, DoctorDTO>()).CreateMapper();
			mapperDTOToEntity = new MapperConfiguration(cfg => cfg.CreateMap<DoctorDTO, Doctor>()).CreateMapper();
		}

		public IEnumerable<DoctorDTO> GetAll()
		{
			return mapperDTOFromEntity.Map<IEnumerable<Doctor>, List<DoctorDTO>>(DataBase.Doctor.GetAll());
		}

		public DoctorDTO Get(int id)
		{
			var patient = DataBase.Doctor.Get(id);
			if (patient == null)
				throw new ValidationException("Doctor not found", "");

			return mapperDTOFromEntity.Map<Doctor, DoctorDTO>(patient);
		}

		public DoctorDTO Create(DoctorDTO item)
		{
			if (item.DoctorId is not null)
				throw new ValidationException("DoctorId must be empty", "");
			if (item.Surname == "")
				throw new ValidationException("Empty patient surname", "");
			if (item.Name == "")
				throw new ValidationException("Empty patient name", "");
			if (item.Patronymic == "")
				throw new ValidationException("Empty patient patronymic", "");
			var ret = DataBase.Doctor.Create(mapperDTOToEntity.Map<Doctor>(item));
			DataBase.Save();
			return mapperDTOFromEntity.Map<DoctorDTO>(ret);
		}

		public DoctorDTO Update(DoctorDTO item)
		{
			if (item.DoctorId is null)
				throw new ValidationException("Empty patient id", "");
			Doctor patient = DataBase.Doctor.Get((int)item.DoctorId);

			if (patient == null)
				throw new ValidationException("Doctor not found", "");

			patient.Name = item.Name;
			patient.Surname = item.Surname;
			patient.Patronymic = item.Patronymic;

			var ret = DataBase.Doctor.Update(patient);
			DataBase.Save();
			return mapperDTOFromEntity.Map<DoctorDTO>(ret);
		}

		public void Delete(int id)
		{
			DataBase.Doctor.Delete(id);
			DataBase.Save();
		}

		public IEnumerable<DoctorsScheduleDTO> GetSchedule(int id)
		{
			var n = DataBase.Doctor.Get(id);
			if (n == null)
				throw new ValidationException("Doctor not found", "");

			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DoctorsSchedule, DoctorsScheduleDTO>()).CreateMapper();
			return mapper.Map<IEnumerable<DoctorsSchedule>, List<DoctorsScheduleDTO>>(DataBase.Doctor.GetSchedule(id));
		}


		public void AddScheduleElement(DoctorDTO doctor, DoctorsScheduleDTO element)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<AttendanceCardDTO> GetAttendanceCard(int id)
		{
			var n = DataBase.Doctor.Get(id);
			if (n == null)
				throw new ValidationException("Doctor not found", "");

			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<AttendanceCard, AttendanceCardDTO>()).CreateMapper();
			return mapper.Map<IEnumerable<AttendanceCard>, List<AttendanceCardDTO>>(DataBase.Doctor.GetAttendanceCard(id));
		}

		public IEnumerable<DoctorDTO> FindByText(string text)
		{
			return mapperDTOFromEntity.Map<IEnumerable<DoctorDTO>>(DataBase.Doctor.FindByText(text));
		}

		public IEnumerable<DoctorsScheduleDTO> GetReceptionSchedule(int id)
		{
			return GetSchedule(id).Where(n => n.PatientId is not null);
		}

		public IEnumerable<DoctorsScheduleDTO> GetFreeSchedule(int id)
		{

			return GetSchedule(id).Where(n => n.PatientId is null);
		}
	}
}
