﻿using AutoMapper;
using NewNet.BLL.DTO;
using NewNet.BLL.Infrastructure;
using NewNet.BLL.Interfaces;
using NewNet.DAL.Entities;
using NewNet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.BLL.Services
{
	public class PatientService : IPatientService<PatientDTO>
	{
		IUnitOfWork DataBase { get; set; }

		IMapper mapperDTOFromEntity;
		IMapper mapperDTOToEntity;

		public PatientService(IUnitOfWork uow)
		{
			DataBase = uow;
			mapperDTOFromEntity = new MapperConfiguration(cfg => cfg.CreateMap<Patient, PatientDTO>()).CreateMapper();
			mapperDTOToEntity = new MapperConfiguration(cfg => cfg.CreateMap<PatientDTO, Patient>()).CreateMapper();
		}

		public IEnumerable<PatientDTO> GetAll()
		{
			return mapperDTOFromEntity.Map<IEnumerable<Patient>, List<PatientDTO>>(DataBase.Patient.GetAll());
		}

		public PatientDTO Get(int id)
		{
			var patient = DataBase.Patient.Get(id);
			if (patient == null)
				throw new ValidationException("Patient not found", "");

			return mapperDTOFromEntity.Map<Patient, PatientDTO>(patient);
		}

		public PatientDTO Create(PatientDTO item)
		{
			if (item.PatientId is not null)
				throw new ValidationException("PatientId must be empty", "");
			if (item.Surname == "")
				throw new ValidationException("Empty patient surname", "");
			if (item.Name == "")
				throw new ValidationException("Empty patient name", "");
			if (item.Patronymic == "")
				throw new ValidationException("Empty patient patronymic", "");
			var ret = DataBase.Patient.Create(mapperDTOToEntity.Map<Patient>(item));
			DataBase.Save();

			return mapperDTOFromEntity.Map<PatientDTO>(ret);
		}

		public PatientDTO Update(PatientDTO item)
		{
			if (item.PatientId is null)
				throw new ValidationException("Empty patient id", "");
			Patient patient = DataBase.Patient.Get((int)item.PatientId);

			if (patient == null)
				throw new ValidationException("Patient not found", "");

			patient.Name = item.Name;
			patient.Surname = item.Surname;
			patient.Patronymic = item.Patronymic;

			var ret = DataBase.Patient.Update(patient);
			DataBase.Save();
			return mapperDTOFromEntity.Map<PatientDTO>(ret);
		}

		public void Delete(int id)
		{
			DataBase.Patient.Delete(id);
			DataBase.Save();
		}

		public IEnumerable<DoctorsScheduleDTO> GetSchedule(int id)
		{
			var n = DataBase.Patient.Get(id);
			if (n == null)
				throw new ValidationException("Patient not found", "");

			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DoctorsSchedule, DoctorsScheduleDTO>()).CreateMapper();
			return mapper.Map<IEnumerable<DoctorsSchedule>, List<DoctorsScheduleDTO>>(DataBase.Patient.GetSchedule(id));
		}


		public IEnumerable<AttendanceCardDTO> GetAttendanceCard(int id)
		{
			var n = DataBase.Patient.Get(id);
			if (n == null)
				throw new ValidationException("Patient not found", "");

			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<AttendanceCard, AttendanceCardDTO>()).CreateMapper();
			return mapper.Map<IEnumerable<AttendanceCard>, List<AttendanceCardDTO>>(DataBase.Patient.GetAttendanceCard(id));
		}

		public IEnumerable<PatientDTO> FindByText(string text)
		{
			return mapperDTOFromEntity.Map<IEnumerable<PatientDTO>>(DataBase.Patient.FindByText(text));
		}
	}
}