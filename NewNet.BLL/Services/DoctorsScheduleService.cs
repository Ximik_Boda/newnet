﻿using AutoMapper;
using NewNet.BLL.DTO;
using NewNet.BLL.Infrastructure;
using NewNet.BLL.Interfaces;
using NewNet.DAL.Entities;
using NewNet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.BLL.Services
{
	public class DoctorsScheduleService : IDoctorsScheduleService<DoctorsScheduleDTO>
	{
		IUnitOfWork DataBase { get; set; }

		IMapper mapperDTOFromEntity;
		IMapper mapperDTOToEntity;

		public DoctorsScheduleService(IUnitOfWork uow)
		{
			DataBase = uow;
			mapperDTOFromEntity = new MapperConfiguration(cfg => cfg.CreateMap<DoctorsSchedule, DoctorsScheduleDTO>()).CreateMapper();
			mapperDTOToEntity = new MapperConfiguration(cfg => cfg.CreateMap<DoctorsScheduleDTO, DoctorsSchedule>()).CreateMapper();
		}

		public IEnumerable<DoctorsScheduleDTO> GetAll()
		{
			return mapperDTOFromEntity.Map<IEnumerable<DoctorsSchedule>, List<DoctorsScheduleDTO>>(DataBase.DoctorsSchedule.GetAll());
		}

		public DoctorsScheduleDTO Get(int id)
		{
			var doctorsSchedule = DataBase.DoctorsSchedule.Get(id);
			if (doctorsSchedule is null)
				throw new ValidationException("DoctorsSchedule not found", "");

			return mapperDTOFromEntity.Map<DoctorsSchedule, DoctorsScheduleDTO>(doctorsSchedule);
		}

		public DoctorsScheduleDTO Create(DoctorsScheduleDTO item)
		{
			if (item.ScheduleId is not null)
				throw new ValidationException("ScheduleId must be empty", "");

			if (item.Date is null)
				item.Date = DateTime.Now;

			var ret = DataBase.DoctorsSchedule.Create(mapperDTOToEntity.Map<DoctorsSchedule>(item));
			DataBase.Save();

			return mapperDTOFromEntity.Map<DoctorsScheduleDTO>(ret);
		}

		public DoctorsScheduleDTO Update(DoctorsScheduleDTO item)
		{
			if (item.ScheduleId is null)
				throw new ValidationException("Empty doctorsSchedule id", "");
			DoctorsSchedule doctorsSchedule = DataBase.DoctorsSchedule.Get((int)item.ScheduleId);

			if (doctorsSchedule is null)
				throw new ValidationException("DoctorsSchedule not found", "");

			if (item.Date is null)
				item.Date = DateTime.Now;
			else 
				doctorsSchedule.Date = (DateTime)item.Date;

			doctorsSchedule.DoctorId = item.DoctorId;
			doctorsSchedule.PatientId = item.PatientId;

			var ret = DataBase.DoctorsSchedule.Update(doctorsSchedule);
			DataBase.Save();
			return mapperDTOFromEntity.Map<DoctorsScheduleDTO>(ret);
		}

		public void Delete(int id)
		{
			DataBase.DoctorsSchedule.Delete(id);
			DataBase.Save();
		}

		public DoctorsScheduleDTO PatientUser(int id, int patient_id)
		{
			DoctorsSchedule doctorsSchedule = DataBase.DoctorsSchedule.Get(id);
			Patient patient = DataBase.Patient.Get(patient_id);

			if (doctorsSchedule is null)
				throw new ValidationException("DoctorsSchedule not found", "");

			if (patient is null)
				throw new ValidationException("DoctorsSchedule not found", "");

			if (doctorsSchedule.PatientId is not null)
				throw new ValidationException("Patient is already set", "");

			doctorsSchedule.PatientId = patient_id;

			var ret = DataBase.DoctorsSchedule.Update(doctorsSchedule);

			DataBase.Save();
			return mapperDTOFromEntity.Map<DoctorsScheduleDTO>(ret);
		}

		public void DeletePatient(int id)
		{
			DoctorsSchedule doctorsSchedule = DataBase.DoctorsSchedule.Get(id);

			if (doctorsSchedule is null)
				throw new ValidationException("DoctorsSchedule not found", "");

			if (doctorsSchedule.PatientId is null)
				throw new ValidationException("Patient is already deleted", "");

			doctorsSchedule.PatientId = null;

			var ret = DataBase.DoctorsSchedule.Update(doctorsSchedule);
			DataBase.Save();
		}
	}
}
