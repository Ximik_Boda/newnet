﻿using AutoMapper;
using NewNet.BLL.DTO;
using NewNet.BLL.Infrastructure;
using NewNet.BLL.Interfaces;
using NewNet.DAL.Entities;
using NewNet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.BLL.Services
{
	public class AttendanceCardService : IService<AttendanceCardDTO>
	{
		IUnitOfWork DataBase { get; set; }

		IMapper mapperDTOFromEntity;
		IMapper mapperDTOToEntity;

		public AttendanceCardService(IUnitOfWork uow)
		{
			DataBase = uow;
			mapperDTOFromEntity = new MapperConfiguration(cfg => cfg.CreateMap<AttendanceCard, AttendanceCardDTO>()).CreateMapper();
			mapperDTOToEntity = new MapperConfiguration(cfg => cfg.CreateMap<AttendanceCardDTO, AttendanceCard>()).CreateMapper();
		}

		public IEnumerable<AttendanceCardDTO> GetAll()
		{
			return mapperDTOFromEntity.Map<IEnumerable<AttendanceCard>, List<AttendanceCardDTO>>(DataBase.AttendanceCard.GetAll());
		}

		public AttendanceCardDTO Get(int id)
		{
			var attendanceCard = DataBase.AttendanceCard.Get(id);
			if (attendanceCard == null)
				throw new ValidationException("AttendanceCard not found", "");

			return mapperDTOFromEntity.Map<AttendanceCard, AttendanceCardDTO>(attendanceCard);
		}

		public AttendanceCardDTO Create(AttendanceCardDTO item)
		{
			if (item.AttendanceCardId is not null)
				throw new ValidationException("AttendanceCardId must be empty", "");
			if (item.Diagnos == "")
				throw new ValidationException("Empty attendance card diagnos", "");

			if (item.Date is null)
				item.Date = DateTime.Now;

			var ret = DataBase.AttendanceCard.Create(mapperDTOToEntity.Map<AttendanceCard>(item));
			DataBase.Save();
			return mapperDTOFromEntity.Map<AttendanceCardDTO>(ret);
		}

		public AttendanceCardDTO Update(AttendanceCardDTO item)
		{
			if (item.AttendanceCardId is null)
				throw new ValidationException("Empty attendance card id", "");
			AttendanceCard attendanceCard = DataBase.AttendanceCard.Get((int)item.AttendanceCardId);

			if (attendanceCard == null)
				throw new ValidationException("Attendance card not found", "");

			if (item.Date is null)
				item.Date = DateTime.Now;
			else
				attendanceCard.Date = (DateTime)item.Date;

			attendanceCard.Diagnos = item.Diagnos;
			attendanceCard.PatientId = item.PatientId;
			attendanceCard.DoctorId = item.DoctorId;

			var ret = DataBase.AttendanceCard.Update(attendanceCard);
			DataBase.Save();
			return mapperDTOFromEntity.Map<AttendanceCardDTO>(ret);
		}

		public void Delete(int id)
		{
			DataBase.AttendanceCard.Delete(id);
			DataBase.Save();
		}

	}
}
