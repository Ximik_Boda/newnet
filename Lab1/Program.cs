﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyList;

namespace Lab1
{
	class Program
	{
		static void OnClearHandler(object sender)
		{
			Console.WriteLine("OnClearHandler is called ({0})", sender.ToString());
		}

		static void OnAddHandler(object sender, OnAddDeleteEventArgs<int> e)
		{
			Console.WriteLine("OnAddHandler is called, adding {0} element to {1} index ({2})", e.el, e.index, sender.ToString());
		}

		static void OnDeleteHandler(object sender, OnAddDeleteEventArgs<int> e)
		{
			Console.WriteLine("OnDeleteHandler is called, delete {0} element from {1} index ({2})", e.el, e.index, sender.ToString());
		}

		static void OnAccesToFirstItemHandler(object sender)
		{
			Console.WriteLine("OnAccesToFirstItemHandler is called ({0})", sender.ToString());
		}
		static void OnAccesToLastItemHandler(object sender)
		{
			Console.WriteLine("OnAccesToLastItemHandler is called ({0})", sender.ToString());
		}
		static void OnChangeOffsetHandler(object sender, OnChangeOffsetEventArgs e)
		{
			Console.WriteLine("OnChangeOffsetHandler is called, new offset is {0} ({1})", e.offset, sender.ToString());
		}

		static void PrintMyListInt(MyList<int> l)
		{
			Console.WriteLine("Size = {0}, offset = {1}", l.Count, l.Offset);
			Console.WriteLine("Values: ");
			foreach (int v in l)
			{
				Console.Write("{0}, ", v);
			}
			Console.WriteLine();
		}

		static void Main(string[] args)
		{
			Random rnd = new Random();

			//List<int>

			Console.WriteLine("Test MyList");

			MyList<int> a;

			try
			{
				Console.WriteLine("Trying to init MyList (StartIndex (offset) = 10)");
				a = new MyList<int>(10);
				Console.WriteLine("Successful");
			}
			catch (Exception e)
			{
				if (e.Source != null)
					Console.WriteLine("Exception source: {0}", e.Source);
				throw;
			}
			Console.WriteLine(); // Adding element

			Console.WriteLine("Adding element 2 to the my list");
			a.Add(2);

			Console.WriteLine("Adding element 3 to the my list");
			a.Add(3);

			Console.WriteLine(); // Trying to access to some element element
			try
			{
				Console.WriteLine("Trying to access to element 1");
				Console.WriteLine("a[1]={0}", a[1]);
			}
			catch
			{
				Console.WriteLine("Error");
			}
			try
			{
				Console.WriteLine("Trying to access to element 11");
				Console.WriteLine("a[11]={0}", a[11]);
			}
			catch
			{
				Console.WriteLine("Error");
			}

			Console.WriteLine();
			Console.WriteLine("Test foreach");

			foreach (int v in a)
			{
				Console.Write("{0} ", v);
			}
			Console.WriteLine();
			Console.WriteLine();

			Console.WriteLine("Adding events delegats");

			a.OnClear += OnClearHandler;
			//a.OnAdd += OnAddHandler;
			a.OnDelete += OnDeleteHandler;
			a.OnAccesToFirstItem += OnAccesToFirstItemHandler;
			a.OnAccesToLastItem += OnAccesToLastItemHandler;
			a.OnChangeOffset += OnChangeOffsetHandler;

			Console.WriteLine("Clear MyList");
			a.Clear();

			Console.WriteLine();

			Console.WriteLine("Set offset = -10");
			a.Offset = -10;

			Console.WriteLine();

			Console.WriteLine("Adding some random element");
			for (int i = 0; i < 10; ++i)
				a.Add(rnd.Next(0, 20));

			Console.WriteLine();
			PrintMyListInt(a);
			Console.WriteLine();

			Console.WriteLine("Inserting some random element to rand pos");
			for (int i = 0; i < 10; ++i)
				a.Insert(rnd.Next(a.Offset, a.Count + a.Offset), rnd.Next(0, 20));

			Console.WriteLine();
			PrintMyListInt(a);
			Console.WriteLine();

			Console.WriteLine("Deleting some random element");
			for (int i = 0; i < 10; ++i)
				a.RemoveAt(rnd.Next(a.Offset, a.Count + a.Offset));

			Console.WriteLine();
			PrintMyListInt(a);
			Console.WriteLine();

			{
				Console.WriteLine("Finding all element (el%3==0)");
				MyList<int> result = a.FindAll(
					delegate (int el)
					{
						return (bool)(el % 3 == 0);
					});
				Console.WriteLine();
				PrintMyListInt(result);
				Console.WriteLine();
			}

			{
				Console.WriteLine("Get range without first 2 and end 2 elements");
				MyList<int> result = a.GetRange(a.Offset + 2, a.Count - 4);
				Console.WriteLine();
				PrintMyListInt(result);
				Console.WriteLine();
			}

			Console.ReadKey();
		}
	}
}
