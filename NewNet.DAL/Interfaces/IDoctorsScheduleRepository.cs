﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.DAL.Interfaces
{
	public interface IDoctorsScheduleRepository<DoctorsSchedule> : IRepository<DoctorsSchedule>
	{
	}
}
