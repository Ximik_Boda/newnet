﻿using NewNet.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<AttendanceCard> AttendanceCard { get; }
        IDoctorRepository<Doctor> Doctor { get; }
        IRepository<DoctorsSchedule> DoctorsSchedule { get; }
        IPatientRepository<Patient> Patient { get; }
        void Save();
    }
}
