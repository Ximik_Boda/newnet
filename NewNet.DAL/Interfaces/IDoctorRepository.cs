﻿using NewNet.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.DAL.Interfaces
{
	public interface IDoctorRepository<Doctor> : IRepository<Doctor>
	{
		void AddScheduleElement(Doctor doctor, DoctorsSchedule element);
		IEnumerable<DoctorsSchedule> GetSchedule(int id);
		IEnumerable<AttendanceCard> GetAttendanceCard(int id);

		IEnumerable<Doctor> FindByText(string find);


	}
}
