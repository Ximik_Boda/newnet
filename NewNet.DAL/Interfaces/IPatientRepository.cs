﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewNet.DAL.Entities;

namespace NewNet.DAL.Interfaces
{
	public interface IPatientRepository<Patient> : IRepository<Patient>
	{
		IEnumerable<DoctorsSchedule> GetSchedule(int id);
		IEnumerable<AttendanceCard> GetAttendanceCard(int id);

		IEnumerable<Patient> FindByText(string find);


	}
}
