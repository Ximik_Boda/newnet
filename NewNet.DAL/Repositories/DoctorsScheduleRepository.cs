﻿using Microsoft.EntityFrameworkCore;
using NewNet.DAL.Context;
using NewNet.DAL.Entities;
using NewNet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.DAL.Repositories
{
    public class DoctorsScheduleRepository : IRepository<DoctorsSchedule>
    {
        private HospitalRegistryContext db;

        public DoctorsScheduleRepository(HospitalRegistryContext context)
        {
            db = context;
        }

        public IEnumerable<DoctorsSchedule> GetAll()
        {
            return db.DoctorsSchedule;
        }

        public DoctorsSchedule Get(int id)
        {
            return db.DoctorsSchedule.Find(id);
        }

        public DoctorsSchedule Create(DoctorsSchedule doctorsSchedule)
        {
            return db.DoctorsSchedule.Add(doctorsSchedule).Entity;
        }

        public DoctorsSchedule Update(DoctorsSchedule doctorsSchedule)
        {
            var el = db.Entry(doctorsSchedule);
            el.State = EntityState.Modified;
            return el.Entity;
        }

        public IEnumerable<DoctorsSchedule> Find(Func<DoctorsSchedule, Boolean> predicate)
        {
            return db.DoctorsSchedule.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            DoctorsSchedule doctorsSchedule = db.DoctorsSchedule.Find(id);
            if (doctorsSchedule != null)
                db.DoctorsSchedule.Remove(doctorsSchedule);
        }
    }
}
