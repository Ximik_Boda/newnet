﻿using Microsoft.EntityFrameworkCore;
using NewNet.DAL.Context;
using NewNet.DAL.Entities;
using NewNet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.DAL.Repositories
{
    public class AttendanceCardRepository : IRepository<AttendanceCard>
    {
        private HospitalRegistryContext db;

        public AttendanceCardRepository(HospitalRegistryContext context)
        {
            db = context;
        }

        public IEnumerable<AttendanceCard> GetAll()
        {
            return db.AttendanceCard;
        }

        public AttendanceCard Get(int id)
        {
            return db.AttendanceCard.Find(id);
        }

        public AttendanceCard Create(AttendanceCard attendanceCard)
        {
            return db.AttendanceCard.Add(attendanceCard).Entity;
        }

        public AttendanceCard Update(AttendanceCard attendanceCard)
        {
            var el = db.Entry(attendanceCard);
            el.State = EntityState.Modified;
            return el.Entity;
        }

        public IEnumerable<AttendanceCard> Find(Func<AttendanceCard, Boolean> predicate)
        {
            return db.AttendanceCard.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            AttendanceCard attendanceCard = db.AttendanceCard.Find(id);
            if (attendanceCard != null)
                db.AttendanceCard.Remove(attendanceCard);
        }
    }
}
