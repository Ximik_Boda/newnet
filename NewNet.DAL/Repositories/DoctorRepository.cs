﻿using Microsoft.EntityFrameworkCore;
using NewNet.DAL.Context;
using NewNet.DAL.Entities;
using NewNet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.DAL.Repositories
{
	public class DoctorRepository : IDoctorRepository<Doctor>
	{
		private HospitalRegistryContext db;

		public DoctorRepository(HospitalRegistryContext context)
		{
			db = context;
		}

		public IEnumerable<Doctor> GetAll()
		{
			return db.Doctor;
		}

		public Doctor Get(int id)
		{
			return db.Doctor.Find(id);
		}

		public Doctor Create(Doctor doctor)
		{
			return db.Doctor.Add(doctor).Entity;
		}

		public Doctor Update(Doctor doctor)
		{
			var el = db.Entry(doctor);
			el.State = EntityState.Modified;
			return el.Entity;
		}

		public IEnumerable<Doctor> Find(Func<Doctor, Boolean> predicate)
		{
			return db.Doctor.Where(predicate).ToList();
		}

		public void Delete(int id)
		{
			Doctor doctor = db.Doctor.Find(id);
			if (doctor != null)
				db.Doctor.Remove(doctor);
		}

		public void AddScheduleElement(Doctor doctor, DoctorsSchedule element)
		{
			doctor.Schedule.Add(element);

		}

		public IEnumerable<DoctorsSchedule> GetSchedule(int id)
		{
			return db.Doctor.Include(n => n.Schedule).Where(t => t.DoctorId == id).FirstOrDefault().Schedule;
		}
		public IEnumerable<AttendanceCard> GetAttendanceCard(int id)
		{
			return db.Doctor.Include(n => n.AttendanceCard).Where(t => t.DoctorId == id).FirstOrDefault().AttendanceCard;
		}

		public IEnumerable<Doctor> FindByText(string find)
		{
			return db.Doctor.Where(t => t.Surname.Contains(find) || t.Name.Contains(find)
			|| t.Patronymic.Contains(find)).ToList();

		}

	}
}
