﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NewNet.DAL.Context;
using NewNet.DAL.Entities;
using NewNet.DAL.Interfaces;

namespace NewNet.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed = false;

        private HospitalRegistryContext db;

		public IRepository<AttendanceCard> _attendanceCard;
		public IDoctorRepository<Doctor> _doctor;
		public IRepository<DoctorsSchedule> _doctorsSchedule;
		public IPatientRepository<Patient> _patient;

		public UnitOfWork(DbContextOptions<HospitalRegistryContext> options)
        {
            db = new HospitalRegistryContext(options);
        }

        public IRepository<AttendanceCard> AttendanceCard => _attendanceCard ??= new AttendanceCardRepository(db);
        public IDoctorRepository<Doctor> Doctor => _doctor ??= new DoctorRepository(db);
        public IRepository<DoctorsSchedule> DoctorsSchedule => _doctorsSchedule ??= new DoctorsScheduleRepository(db);
        public IPatientRepository<Patient> Patient => _patient ??= new PatientRepository(db);

        public void Save()
        {
            db.SaveChanges();
        }


        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                    db.Dispose();

                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
