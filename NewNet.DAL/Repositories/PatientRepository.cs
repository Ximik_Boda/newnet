﻿using Microsoft.EntityFrameworkCore;
using NewNet.DAL.Context;
using NewNet.DAL.Entities;
using NewNet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewNet.DAL.Repositories
{
    public class PatientRepository : IPatientRepository<Patient>
    {
        private HospitalRegistryContext db;

        public PatientRepository(HospitalRegistryContext context)
        {
            db = context;
        }

        public IEnumerable<Patient> GetAll()
        {
            return db.Patient;
        }

        public Patient Get(int id)
        {
            return db.Patient.Find(id);
        }

        public Patient Create(Patient patient)
        {
            return db.Patient.Add(patient).Entity;
        }

        public Patient Update(Patient patient)
        {
            var el = db.Entry(patient);
            el.State = EntityState.Modified;
            return el.Entity;
        }

        public IEnumerable<Patient> Find(Func<Patient, Boolean> predicate)
        {
            return db.Patient.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            Patient patient = db.Patient.Find(id);
            if (patient != null)
                db.Patient.Remove(patient);
        }

        public IEnumerable<DoctorsSchedule> GetSchedule(int id)
        {
            return db.Patient.Include(n => n.Schedule).Where(t => t.PatientId == id).FirstOrDefault().Schedule;
        }
        public IEnumerable<AttendanceCard> GetAttendanceCard(int id)
        {
            return db.Patient.Include(n => n.AttendanceCard).Where(t => t.PatientId == id).FirstOrDefault().AttendanceCard;
        }

        public IEnumerable<Patient> FindByText(string find)
        {
            return db.Patient.Where(t => t.Surname.Contains(find) || t.Name.Contains(find)
            || t.Patronymic.Contains(find)).ToList();

        }
    }
}
