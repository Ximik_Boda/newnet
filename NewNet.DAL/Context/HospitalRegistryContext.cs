﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NewNet.DAL.Entities;

namespace NewNet.DAL.Context
{
	public class HospitalRegistryContext : DbContext
	{
		public HospitalRegistryContext(DbContextOptions<HospitalRegistryContext> options)
			: base(options) 
		{
			Database.EnsureCreated();
		}
		public DbSet<Doctor> Doctor { get; set; }
		public DbSet<Patient> Patient { get; set; }
		public DbSet<AttendanceCard> AttendanceCard { get; set; }
		public DbSet<DoctorsSchedule> DoctorsSchedule { get; set; }
	}
}
