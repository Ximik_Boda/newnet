﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewNet.DAL.Entities
{
	public class DoctorsSchedule
	{
		[Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int ScheduleId { get; set; }

		[Required] public DateTime Date { get; set; }

		public int? DoctorId { get; set; }
		[Required] public Doctor Doctor { get; set; }

		public int? PatientId { get; set; }
		public Patient Patient { get; set; }
	}
}
