﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewNet.DAL.Entities
{
	public class Patient
	{
		[Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int PatientId { get; set; }

		[Required] [MaxLength(50)] public string Surname { get; set; }
		[Required] [MaxLength(50)] public string Name { get; set; }
		[Required] [MaxLength(50)] public string Patronymic { get; set; }

		public ICollection<DoctorsSchedule> Schedule { get; set; }
		public ICollection<AttendanceCard> AttendanceCard { get; set; }

		public Patient()
		{
			Schedule = new List<DoctorsSchedule>();
			AttendanceCard = new List<AttendanceCard>();
		}
	}
}
