﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewNet.DAL.Entities
{
	public class AttendanceCard
	{
		[Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int AttendanceCardId { get; set; }

		[Required] public DateTime Date { get; set; }

		[Required] [MaxLength(250)] public string Diagnos { get; set; }

		public int? DoctorId { get; set; }
		[Required] public Doctor Doctor { get; set; }
		public int? PatientId { get; set; }
		[Required] public Patient Patient { get; set; }
	}
}
