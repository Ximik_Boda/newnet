using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using NewNet.BLL.DTO;
using NewNet.BLL.Interfaces;
using NewNet.BLL.Services;
using NewNet.DAL.Context;
using NewNet.DAL.Interfaces;
using NewNet.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewNet.PL
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{

			services.AddControllers();

			services.AddScoped<IPatientService<PatientDTO>, PatientService>();
			services.AddScoped<IDoctorService<DoctorDTO>, DoctorService>();
			services.AddScoped<IDoctorsScheduleService<DoctorsScheduleDTO>, DoctorsScheduleService>();
			services.AddScoped<IService<AttendanceCardDTO>, AttendanceCardService>();

			var optionsBuilder = new DbContextOptionsBuilder<HospitalRegistryContext>();

			var options = optionsBuilder
					.UseSqlServer(Configuration.GetConnectionString("DBConnection"))
					.Options;

			services.AddScoped<IUnitOfWork, UnitOfWork>(s => new UnitOfWork(options));

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "NewNet.PL", Version = "v1" });
				c.EnableAnnotations();
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseSwagger();
				app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "NewNet.PL v1"));
			}


			app.UseDefaultFiles();
			app.UseStaticFiles();

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
