﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NewNet.BLL.DTO;
using NewNet.BLL.Infrastructure;
using NewNet.BLL.Interfaces;
using NewNet.PL.Models;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewNet.PL.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class DoctorsScheduleController : Controller
	{
		IDoctorsScheduleService<DoctorsScheduleDTO> doctorsScheduleService;

		IMapper mapperModelFromDTO;
		IMapper mapperModelToDTO;
		public DoctorsScheduleController(IDoctorsScheduleService<DoctorsScheduleDTO> serv)
		{
			doctorsScheduleService = serv;
			mapperModelFromDTO = new MapperConfiguration(cfg => cfg.CreateMap<DoctorsScheduleDTO, DoctorsScheduleModel>()).CreateMapper();
			mapperModelToDTO = new MapperConfiguration(cfg => cfg.CreateMap<DoctorsScheduleModel, DoctorsScheduleDTO>()).CreateMapper();
		}

		[HttpGet]
		public IActionResult Get()
		{
			return Ok(mapperModelFromDTO.Map<IEnumerable<DoctorsScheduleDTO>, List<DoctorsScheduleModel>>(doctorsScheduleService.GetAll()));
		}

		[HttpGet("{id}")]
		[SwaggerResponse(200)]
		public IActionResult Get(int id)
		{
			try
			{
				var rubricItem = doctorsScheduleService.Get(id);

				return Ok(mapperModelFromDTO.Map<DoctorsScheduleDTO, DoctorsScheduleModel>(rubricItem));
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}
		}

		[HttpPost]
		[SwaggerResponse(400, "The product data is invalid")]
		public IActionResult Post([FromBody] DoctorsScheduleModel item)
		{
			try
			{
				var ret = doctorsScheduleService.Create(mapperModelToDTO.Map<DoctorsScheduleModel, DoctorsScheduleDTO>(item));

				return StatusCode(201, mapperModelFromDTO.Map<DoctorsScheduleDTO, DoctorsScheduleModel>(ret));
			}
			catch (Exception ex)
			{
				return StatusCode(400, ex.Message);
			}
		}

		[HttpPut]
		public IActionResult Put([FromBody] DoctorsScheduleModel item)
		{
			if (item.ScheduleId is null)
			{
				return Post(item);
			}

			try
			{
				var ret = doctorsScheduleService.Update(mapperModelToDTO.Map<DoctorsScheduleDTO>(item));

				return Ok(mapperModelFromDTO.Map<DoctorsScheduleDTO, DoctorsScheduleModel>(ret));
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}
		}

		[HttpDelete("{id}")]
		public IActionResult Delete(int id)
		{
			try
			{
				var rubricItem = doctorsScheduleService.Get(id);
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}

			doctorsScheduleService.Delete(id);
			return NoContent();
		}

		[HttpPut("{id}/patient")]
		public IActionResult PutUser(int id, [FromQuery] int patient_id)
		{
			try
			{
				var ret = doctorsScheduleService.PatientUser(id, patient_id);

				return Ok(mapperModelFromDTO.Map<DoctorsScheduleDTO, DoctorsScheduleModel>(ret));
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}
		}

		[HttpDelete("{id}/patient")]
		public IActionResult DeletePatient(int id)
		{
			try
			{
				doctorsScheduleService.DeletePatient(id);
				return NoContent();
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}
		}
	}
}
