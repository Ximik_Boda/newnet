﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NewNet.BLL.DTO;
using NewNet.BLL.Infrastructure;
using NewNet.BLL.Interfaces;
using NewNet.PL.Models;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewNet.PL.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class AttendanceCardController : Controller
	{
		IService<AttendanceCardDTO> attendanceCardService;

		IMapper mapperModelFromDTO;
		IMapper mapperModelToDTO;
		public AttendanceCardController(IService<AttendanceCardDTO> serv)
		{
			attendanceCardService = serv;
			mapperModelFromDTO = new MapperConfiguration(cfg => cfg.CreateMap<AttendanceCardDTO, AttendanceCardModel>()).CreateMapper();
			mapperModelToDTO = new MapperConfiguration(cfg => cfg.CreateMap<AttendanceCardModel, AttendanceCardDTO>()).CreateMapper();
		}

		[HttpGet]
		public IActionResult Get()
		{
			return Ok(mapperModelFromDTO.Map<IEnumerable<AttendanceCardDTO>, List<AttendanceCardModel>>(attendanceCardService.GetAll()));
		}

		[HttpGet("{id}")]
		[SwaggerResponse(200)]
		public IActionResult Get(int id)
		{
			try
			{
				var rubricItem = attendanceCardService.Get(id);

				return Ok(mapperModelFromDTO.Map<AttendanceCardDTO, AttendanceCardModel>(rubricItem));
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}
		}

		[HttpPost]
		[SwaggerResponse(400, "The product data is invalid")]
		public IActionResult Post([FromBody] AttendanceCardModel item)
		{
			try
			{
				var ret = attendanceCardService.Create(mapperModelToDTO.Map<AttendanceCardModel, AttendanceCardDTO>(item));

				return StatusCode(201, mapperModelFromDTO.Map<AttendanceCardDTO, AttendanceCardModel>(ret));

			}
			catch (Exception ex)
			{
				return StatusCode(400, ex.Message);
			}
		}

		[HttpPut]
		public IActionResult Put([FromBody] AttendanceCardModel item)
		{
			if (item.AttendanceCardId is null)
			{
				return Post(item);
			}

			try
			{
				var ret = attendanceCardService.Update(mapperModelToDTO.Map<AttendanceCardDTO>(item));

				return Ok(mapperModelFromDTO.Map<AttendanceCardDTO, AttendanceCardModel>(ret));
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}
		}

		[HttpDelete("{id}")]
		public IActionResult Delete(int id)
		{
			try
			{
				var rubricItem = attendanceCardService.Get(id);
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}

			attendanceCardService.Delete(id);
			return NoContent();
		}
	}
}
