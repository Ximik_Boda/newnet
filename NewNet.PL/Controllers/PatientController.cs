﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NewNet.BLL.DTO;
using NewNet.BLL.Infrastructure;
using NewNet.BLL.Interfaces;
using NewNet.PL.Models;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewNet.PL.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class PatientController : Controller
	{
		IPatientService<PatientDTO> patientService;

		IMapper mapperModelFromDTO;
		IMapper mapperModelToDTO;
		public PatientController(IPatientService<PatientDTO> serv)
		{
			patientService = serv;
			mapperModelFromDTO = new MapperConfiguration(cfg => cfg.CreateMap<PatientDTO, PatientModel>()).CreateMapper();
			mapperModelToDTO = new MapperConfiguration(cfg => cfg.CreateMap<PatientModel, PatientDTO > ()).CreateMapper();
		}

		[HttpGet]
		public IActionResult Get()
		{
			return Ok(mapperModelFromDTO.Map<IEnumerable<PatientDTO>, List<PatientModel>>(patientService.GetAll()));
		}

		[HttpGet("{id}")]
		[SwaggerResponse(200)]
		public IActionResult Get(int id)
		{
			try
			{
				var rubricItem = patientService.Get(id);

				return Ok(mapperModelFromDTO.Map<PatientDTO, PatientModel>(rubricItem));
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}
		}

		[HttpPost]
		[SwaggerResponse(400, "The product data is invalid")]
		public IActionResult Post([FromBody] PatientModel item)
		{
			try
			{
				var ret = patientService.Create(mapperModelToDTO.Map<PatientModel, PatientDTO>(item));

				return StatusCode(201, mapperModelFromDTO.Map<PatientDTO, PatientModel>(ret));
			}
			catch (Exception ex)
			{
				return StatusCode(400, ex.Message);
			}
		}

		[HttpPut]
		public IActionResult Put([FromBody] PatientModel item)
		{
			if (item.PatientId is null)
			{
				return Post(item);
			}

			try
			{
				var ret = patientService.Update(mapperModelToDTO.Map<PatientDTO>(item));

				return Ok(mapperModelFromDTO.Map<PatientDTO, PatientModel>(ret));
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}
		}

		[HttpDelete("{id}")]
		public IActionResult Delete(int id)
		{
			try
			{
				var rubricItem = patientService.Get(id);
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}

			patientService.Delete(id);
			return NoContent();
		}

		[HttpGet("find_by_text")]
		public IActionResult FindByText(string text)
		{
			try
			{
				var rubricItem = patientService.FindByText(text);

				return Ok(mapperModelFromDTO.Map<List<DoctorModel>>(rubricItem));
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}
		}

		[Route("{id}/reception_schedule")]
		[HttpGet]
		public IActionResult Schedule(int id)
		{
			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DoctorsScheduleDTO, DoctorsScheduleModel>()).CreateMapper();
			return new ObjectResult(mapper.Map<IEnumerable<DoctorsScheduleDTO>, List<DoctorsScheduleModel>>(patientService.GetSchedule(id)));
		}
		[Route("{id}/attendance_card")]
		[HttpGet]
		public IActionResult AttendanceCard(int id)
		{
			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<AttendanceCardDTO, AttendanceCardModel>()).CreateMapper();
			return new ObjectResult(mapper.Map<IEnumerable<AttendanceCardDTO>, List<AttendanceCardModel>>(patientService.GetAttendanceCard(id)));
		}

	}
}
