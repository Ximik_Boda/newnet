﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NewNet.BLL.DTO;
using NewNet.BLL.Infrastructure;
using NewNet.BLL.Interfaces;
using NewNet.PL.Models;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewNet.PL.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class DoctorController : Controller
	{
		IDoctorService<DoctorDTO> doctorService;

		IMapper mapperModelFromDTO;
		IMapper mapperModelToDTO;
		public DoctorController(IDoctorService<DoctorDTO> serv)
		{
			doctorService = serv;
			mapperModelFromDTO = new MapperConfiguration(cfg => cfg.CreateMap<DoctorDTO, DoctorModel>()).CreateMapper();
			mapperModelToDTO = new MapperConfiguration(cfg => cfg.CreateMap<DoctorModel, DoctorDTO>()).CreateMapper();
		}

		[HttpGet]
		public IActionResult Get()
		{
			return Ok(mapperModelFromDTO.Map<IEnumerable<DoctorDTO>, List<DoctorModel>>(doctorService.GetAll()));
		}

		[HttpGet("{id}")]
		[SwaggerResponse(200)]
		public IActionResult Get(int id)
		{
			try
			{
				var rubricItem = doctorService.Get(id);

				return Ok(mapperModelFromDTO.Map<DoctorDTO, DoctorModel>(rubricItem));
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}
		}

		[HttpPost]
		public IActionResult Post([FromBody] DoctorModel item)
		{
			try
			{
				var ret = doctorService.Create(mapperModelToDTO.Map<DoctorModel, DoctorDTO>(item));

				return StatusCode(201, mapperModelFromDTO.Map<DoctorModel>(ret));
			}
			catch (Exception ex)
			{
				return StatusCode(400, ex.Message);
			}
		}

		[HttpPut]
		public IActionResult Put([FromBody] DoctorModel item)
		{
			if (item.DoctorId is null)
			{
				return Post(item);
			}

			try
			{
				var ret = doctorService.Update(mapperModelToDTO.Map<DoctorDTO>(item));

				return Ok(mapperModelFromDTO.Map<DoctorModel>(ret));
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}
		}

		[HttpDelete("{id}")]
		public IActionResult Delete(int id)
		{
			try
			{
				var rubricItem = doctorService.Get(id);
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}

			doctorService.Delete(id);
			return NoContent();
		}

		[HttpGet("find_by_text")]
		public IActionResult FindByText(string text)
		{
			try
			{
				var rubricItem = doctorService.FindByText(text);

				return Ok(mapperModelFromDTO.Map<List<DoctorModel>>(rubricItem));
			}
			catch (ValidationException ex)
			{
				return StatusCode(404, ex.Message);
			}
		}

		[Route("{id}/schedule")]
		[HttpGet]
		public IActionResult Schedule(int id)
		{
			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DoctorsScheduleDTO, DoctorsScheduleModel>()).CreateMapper();
			return new ObjectResult(mapper.Map<IEnumerable<DoctorsScheduleDTO>, List<DoctorsScheduleModel>>(doctorService.GetSchedule(id)));
		}
		[Route("{id}/attendance_card")]
		[HttpGet]
		public IActionResult AttendanceCard(int id)
		{
			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<AttendanceCardDTO, AttendanceCardModel>()).CreateMapper();
			return new ObjectResult(mapper.Map<IEnumerable<AttendanceCardDTO>, List<AttendanceCardModel>>(doctorService.GetAttendanceCard(id)));
		}

		[Route("{id}/schedule/reception")]
		[HttpGet]
		public IActionResult ReceptionSchedule(int id)
		{
			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DoctorsScheduleDTO, DoctorsScheduleModel>()).CreateMapper();
			return new ObjectResult(mapper.Map<IEnumerable<DoctorsScheduleDTO>, List<DoctorsScheduleModel>>(doctorService.GetReceptionSchedule(id)));
		}

		[Route("{id}/schedule/free")]
		[HttpGet]
		public IActionResult FreeSchedule(int id)
		{
			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DoctorsScheduleDTO, DoctorsScheduleModel>()).CreateMapper();
			return new ObjectResult(mapper.Map<IEnumerable<DoctorsScheduleDTO>, List<DoctorsScheduleModel>>(doctorService.GetFreeSchedule(id)));
		}
	}
}
