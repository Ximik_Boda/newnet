﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NewNet.PL.Models
{
	public class PatientModel
	{
		public int? PatientId { get; set; }

		[Required(ErrorMessage = "Please enter patient surname.")]
		[MaxLength(50, ErrorMessage = "The length of the patient surname must be less than 50 characters.")] 
		public string Surname { get; set; }

		[Required(ErrorMessage = "Please enter patient name.")]
		[MaxLength(50, ErrorMessage = "The length of the patient name must be less than 50 characters.")]
		public string Name { get; set; }

		[Required(ErrorMessage = "Please enter patient patronymic.")]
		[MaxLength(50, ErrorMessage = "The length of the patient patronymic must be less than 50 characters.")]
		public string Patronymic { get; set; }
	}
}
