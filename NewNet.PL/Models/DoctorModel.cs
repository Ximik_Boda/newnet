﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NewNet.PL.Models
{
	public class DoctorModel
	{
		public int? DoctorId { get; set; }

		[Required(ErrorMessage = "Please enter doctor surname.")]
		[MaxLength(50, ErrorMessage = "The length of the patient surname must be less than 50 characters.")]
		public string Surname { get; set; }

		[Required(ErrorMessage = "Please enter doctor name.")]
		[MaxLength(50, ErrorMessage = "The length of the patient name must be less than 50 characters.")]
		public string Name { get; set; }

		[Required(ErrorMessage = "Please enter doctor patronymic.")]
		[MaxLength(50, ErrorMessage = "The length of the patient patronymic must be less than 50 characters.")]
		public string Patronymic { get; set; }
	}
}
