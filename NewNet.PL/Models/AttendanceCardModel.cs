﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NewNet.PL.Models
{
	public class AttendanceCardModel
	{
		public int? AttendanceCardId { get; set; }

		//[Required(ErrorMessage = "Please enter date.")] 
		public DateTime? Date { get; set; }

		[Required(ErrorMessage = "Please enter diagnos.")]
		[MaxLength(30, ErrorMessage = "The length of the diagnos must be less than 30 characters.")]
		public string Diagnos { get; set; }

		[Required(ErrorMessage = "Please enter doctor id.")]
		public int? DoctorId { get; set; }

		[Required(ErrorMessage = "Please enter patient id.")]
		public int? PatientId { get; set; }
	}
}
