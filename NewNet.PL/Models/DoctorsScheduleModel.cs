﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NewNet.PL.Models
{
	public class DoctorsScheduleModel
	{
		public int? ScheduleId { get; set; }

		//[Required(ErrorMessage = "Please enter date.")]
		public DateTime? Date { get; set; }

		[Required(ErrorMessage = "Please enter doctor id.")]
		public int DoctorId { get; set; }

		public int? PatientId { get; set; }
	}
}
