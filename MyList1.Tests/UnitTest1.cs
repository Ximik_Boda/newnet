using NUnit.Framework;
using System;
using MyList;
using System.Collections.Generic;

namespace MyList1.Tests
{
	public class Tests
	{
		[SetUp]
		public void Setup()
		{

		}

		[Test]
		public void Constructor_TryToInitialzationWithoutOffset_noException()
		{
			//Arrange

			//Act
			MyList<int> a = new MyList<int>();

			//Assert
			Assert.Pass();
		}

		[Test]
		public void Constructor_TryToInitialzationWithOffset_noException()
		{
			//Arrange

			//Act
			MyList<int> a = new MyList<int>(10);

			//Assert
			Assert.Pass();
		}

		[Test]
		public void Constructor_TryToInitialzationFromAnotherMyList_IsEqualWithSource()
		{
			//Arrange

			//Act
			MyList<int> a = new MyList<int>(10);
			MyList<int> b = new MyList<int>(a);

			//Assert
			Assert.AreEqual(a, b);
		}

		[Test]
		public void Constructor_TryToInitialzationFromNull_ShouldThrowArgumentNullException()
		{
			//Arrange
			//Act
			//Assert
			Assert.Throws<ArgumentNullException>(
				delegate { new MyList<int>(null); });
		}

		[Test]
		public void Add_AddSomeElements_ContentIsCorrect()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.Add(3);
			a.Add(4);

			//Assert
			Assert.AreEqual(a.ToArray(), new int[] { 3, 4 });
		}

		[Test]
		public void Add_AddSomeElementsWithEvent_EventsIsCalledCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			OnAddDeleteEventArgs<int> prog = new OnAddDeleteEventArgs<int>();

			a.OnAdd += delegate (object sender, OnAddDeleteEventArgs<int> e)
			{
				//Assert
				Assert.AreEqual(e.el, prog.el);
				Assert.AreEqual(e.index, prog.index);
			};

			//Act
			prog.el = 3; prog.index = 10;
			a.Add(3);
			prog.el = 4; prog.index = 11;
			a.Add(4);
		}

		[Test]
		public void AddRange_AddSomeElements_ElementsIsAdded()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 4, 6, 3 });

			//Assert
			Assert.AreEqual(a.ToArray(), new int[] { 4, 6, 3 });
		}

		[Test]
		public void Insert_AddSomeToSameValidPosition_ElementsIsInsertedToCorrectPos()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.Insert(10, 1);
			a.Insert(10, 2);
			a.Insert(10, 3);

			//Assert
			Assert.AreEqual(a.ToArray(), new int[] { 3, 2, 1 });
		}
		[Test]
		public void Insert_AddSomeToLastPosition_ElementsIsInsertedToCorrectPos() //like Add
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.Insert(10, 1);
			a.Insert(11, 2);
			a.Insert(12, 3);

			//Assert
			Assert.AreEqual(a.ToArray(), new int[] { 1, 2, 3 });
		}

		[Test]
		public void Insert_TryInvalidSmallPosition_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.Insert(9, 1); });
		}

		[Test]
		public void Insert_TryInvalidBigPosition_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.Insert(11, 1); });
		}

		[Test]
		public void Insert_AddSomeElementsWithEvent_EventsIsCalledCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			OnAddDeleteEventArgs<int> prog = new OnAddDeleteEventArgs<int>();

			a.OnAdd += delegate (object sender, OnAddDeleteEventArgs<int> e)
			{
				//Assert
				Assert.AreEqual(e.el, prog.el);
				Assert.AreEqual(e.index, prog.index);
			};

			//Act
			prog.el = 3; prog.index = 10;
			a.Insert(10, 3);
			prog.el = 4; prog.index = 10;
			a.Insert(10, 4);
			prog.el = 7; prog.index = 11;
			a.Insert(11, 7);
		}

		[Test]
		public void InsertRange_AddSomeElements_ElementsIsInsertedToCorrectPos()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.InsertRange(10, new int[] { 1, 2, 4 });
			a.InsertRange(11, new int[] { 5, 6, 8 });

			//Assert
			Assert.AreEqual(a.ToArray(), new int[] { 1, 5, 6, 8, 2, 4 });
		}

		[Test]
		public void InsertRange_TryInvalidSmallPosition_InvalidOperationException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.InsertRange(9, new int[] { 1, 2, 3 }); });
		}

		[Test]
		public void InsertRange_TryInvalidBigPosition_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.InsertRange(11, new int[] { 1, 2, 3 }); });
		}

		[Test]
		public void Delete_DeleteSomeElements_ElementsIsDeletedCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			a.AddRange(new int[] { 1, 2, 4, 5, 6, 2, 3, 1 });

			//Act
			a.Remove(2);
			a.Remove(1);
			a.Remove(1);
			a.Remove(5);

			//Assert
			Assert.AreEqual(a.ToArray(), new int[] { 4, 6, 2, 3 });
		}

		[Test]
		public void Delete_CheckDeleteStatus_ReturnTrueAndFalse()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3 });

			//Assert
			Assert.IsTrue(a.Remove(2));
			Assert.IsFalse(a.Remove(2));
		}

		[Test]
		public void RemoveAt_DeleteSomeElements_ElementsIsDeletedCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			a.AddRange(new int[] { 1, 2, 3, 4, 5 });

			//Act
			a.RemoveAt(10); // 1
			a.RemoveAt(13); // 5
			a.RemoveAt(11); // 3

			//Assert
			Assert.AreEqual(a.ToArray(), new int[] { 2, 4, });
		}

		[Test]
		public void RemoveAt_DeleteSomeElementsWithEvent_EventsIsCalledCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			a.AddRange(new int[] { 1, 2, 3, 4, 5 });

			OnAddDeleteEventArgs<int> prog = new OnAddDeleteEventArgs<int>();

			a.OnDelete += delegate (object sender, OnAddDeleteEventArgs<int> e)
			{
				//Assert
				Assert.AreEqual(e.el, prog.el);
				Assert.AreEqual(e.index, prog.index);
			};

			//Act
			prog.el = 2; prog.index = 11;
			a.Remove(2);
			prog.el = 5; prog.index = 13;
			a.RemoveAt(13); ;
		}

		[Test]
		public void RemoveAt_TryInvalidSmallPosition_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.RemoveAt(9); });
		}

		[Test]
		public void RemoveAt_TryInvalidBigPosition_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.RemoveAt(10); });
		}

		[Test]
		public void RemoveRange_DeleteSomeRegion_ElementsIsDeletedCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			a.RemoveRange(12, 4); // from 3 to 6

			//Assert
			Assert.AreEqual(new int[] { 1, 2, 7, 8, 9 }, a.ToArray());
		}

		[Test]
		public void RemoveRange_TryInvalidSmallPosition_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.RemoveRange(0, 4); });
		}

		[Test]
		public void RemoveRange_TryInvalidBigPosition_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.RemoveRange(20, 4); });
		}

		[Test]
		public void RemoveRange_TryNegativeCount_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.RemoveRange(10, -4); });
		}

		[Test]
		public void RemoveRange_TryBigCount_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.RemoveRange(10, 10); });
		}

		[Test]
		public void RemoveAll_DeleteSomeElementByPredicate_ElementsIsDeletedCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			a.RemoveAll(delegate (int el) { return el % 2 == 0; });

			//Assert
			Assert.AreEqual(new int[] { 1, 3, 5, 7, 9 }, a.ToArray());
		}

		[Test]
		public void RemoveAll_TrySetNullAsPredicate_ShouldArgumentNullException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			//Assert
			Assert.Throws<ArgumentNullException>(
				delegate { a.RemoveAll(null); ; });
		}

		[Test]
		public void Clear_JustClear_ListIsEmpty()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
			a.Clear();

			//Assert
			Assert.AreEqual(new int[] { }, a.ToArray());
		}

		[Test]
		public void Clear_ClearWithEvent_EventsIsCalledCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Assert
			a.OnClear += delegate (object sender) { Assert.IsTrue(true); };

			//Act
			a.Clear();
		}

		[Test]
		public void GetRange_GetSomeRegion_RegionsIsCorectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			//Assert
			Assert.AreEqual(new int[] { 3, 4, 5, 6 }, a.GetRange(12, 4));
		}

		[Test]
		public void GetRange_TryInvalidSmallPosition_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.GetRange(0, 4); });
		}

		[Test]
		public void GetRange_TryInvalidBigPosition_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.GetRange(20, 4); });
		}

		[Test]
		public void GetRange_TryNegativeCount_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.GetRange(10, -4); });
		}

		[Test]
		public void GetRange_TryBigCount_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a.GetRange(10, 10); });
		}

		[Test]
		public void Contains_CheckElements_ReturnTrueAndFalse()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3 });

			//Assert
			Assert.IsTrue(a.Contains(2));
			Assert.IsFalse(a.Contains(4));
		}

		[Test]
		public void FindAll_FindSomeElementByPredicate_FindedElementsIsCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			//Act
			MyList<int> b = a.FindAll(delegate (int el) { return el % 2 == 0; });

			//Assert
			Assert.AreEqual(new int[] { 2, 4, 6, 8 }, b.ToArray());
		}

		[Test]
		public void FindAll_TrySetNullAsPredicate_ShouldThrowArgumentNullException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			a.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			//Assert
			Assert.Throws<ArgumentNullException>(
				delegate { a.FindAll(null); ; });
		}

		[Test]
		public void ToArray_CheckArrayCorrectly_ArraysAreEqual()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			int[] b = { 1, 2, 3, 10, 2 };

			//Act
			a.AddRange(b);

			int[] c = a.ToArray();

			//Assert
			Assert.AreEqual(b, c);
		}

		[Test]
		public void get_TryToAccessToValidElement_Return2()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 10, 2 });

			//Assert
			Assert.AreEqual(2, a[11]);
		}
		[Test]
		public void get_TryToAccessToInvalidElement_ShouldThrowArgumentOutOfRangeException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.AddRange(new int[] { 1, 2, 3, 10, 2 });

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { int tmp = a[9]; });
		}

		[Test]
		public void set_TryToAccessToValidElement_ArrayTheCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			a.AddRange(new int[] { 1, 2, 3, 10, 2 });

			//Act
			a[11] = 3;

			//Assert
			Assert.AreEqual(new int[] { 1, 3, 3, 10, 2 }, a.ToArray());
		}
		[Test]
		public void set_TryToAccessToInvalidElement_ElementsIsDeletedCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			a.AddRange(new int[] { 1, 2, 3, 10, 2 });

			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				delegate { a[9] = 2; });
		}

		[Test]
		public void OnAccesToFirstItem_TrySetAndGet_EventsIsCalledCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);
			int acceptCount = 0;

			a.AddRange(new int[] { 1, 2, 3, 10, 2 });


			//Act
			a.OnAccesToFirstItem += delegate (object sender)
			{
				++acceptCount;
			};

			a[10] = 5;
			int tmp = a[10];

			//Assert
			Assert.AreEqual(2, acceptCount);
		}
		[Test]
		public void OnAccesToLastItem_TrySetAndGet_EventsIsCalledCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);
			int acceptCount = 0;

			a.AddRange(new int[] { 1, 2, 3, 10, 2 });

			//Act
			a.OnAccesToLastItem += delegate (object sender)
			{
				++acceptCount;
			};

			a[14] = 5;
			int tmp = a[14];

			//Assert
			Assert.AreEqual(2, acceptCount);
		}

		[Test]
		public void getCount_TestEmpty_Return0()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Assert
			Assert.AreEqual(0, a.Count);
		}
		[Test]
		public void getCount_TestOneElements_Return1()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.Add(2);

			//Assert
			Assert.AreEqual(1, a.Count);
		}

		[Test]
		public void getCapacity_TestEmpty_Return0()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Assert
			Assert.AreEqual(0, a.Capacity);
		}

		[Test]
		public void getCapacity_TestOneElements_Return1()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.Add(2);

			//Assert
			Assert.AreEqual(1, a.Capacity);
		}


		[Test]
		public void getOffset_TestReadOffset_Return10()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Assert
			Assert.AreEqual(10, a.Offset);
		}

		[Test]
		public void setOffset_ChangleOffset_Return5()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Act
			a.Add(5);

			a.Offset = 2;

			//Assert
			Assert.AreEqual(5, a[2]);
		}

		[Test]
		public void setOffset_ChangleOffsetWithEvent_EventsIsCalledCorrectly()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			a.OnChangeOffset += delegate (object sender, OnChangeOffsetEventArgs e)
			{
				//Assert
				Assert.AreEqual(2, e.offset);
			};

			//Act
			a.Offset = 2;
		}

		[Test]
		public void getSyncRoot_CheckCorectly_ReturnTheSame ()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Assert
			Assert.AreEqual(a, a.SyncRoot);
		}

		[Test]
		public void getIsSynchronized_CheckStatus_ReturnFalse()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			//Assert
			Assert.IsFalse(a.IsSynchronized);
		}


		[Test]
		public void foreach_test_ArrayTheSame()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);
			MyList<int> b = new MyList<int>(10);

			a.AddRange(new int[] { 1, 2, 3, 10, 2 });

			//Act
			foreach (int el in a) {
				b.Add(el);
			}


			//Assert
			Assert.AreEqual(a.ToArray(), b.ToArray());
		}

		[Test]
		public void enumerator_reset_EnumeratorReturnFirstElement()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			a.AddRange(new int[] { 1, 2, 3, 10, 2 });

			//Act
			var en = a.GetEnumerator();
			en.MoveNext();
			en.Reset();
			en.MoveNext();

			//Assert
			Assert.AreEqual(1, en.Current);
		}

		[Test]
		public void enumerator_wrong_cursor_ShouldThrowInvalidOperationException()
		{
			//Arrange
			MyList<int> a = new MyList<int>(10);

			a.AddRange(new int[] { 1, 2, 3, 10, 2 });

			//Act
			var en = a.GetEnumerator();

			//Assert
			Assert.Throws<InvalidOperationException>(
				delegate { var x = en.Current; });
		}
	}
}